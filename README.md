## Fusion-UNet

Fusion-UNet is an adapted version of the UNet model (Ronneberger et al., 2015) for data fusion. The original architecture is augmented with a second encoding branch and a "virtual" branch for fusion at different levels in feature space.

![](images/fig:funet.png)

This model comes from the paper of Alvarez-Vanhard et al. where the fusion of a digital elevation model (DEM) and multispectral imagery is applied to super-resolution for water level mapping.

## References

O. Ronneberger, P. Fischer, T. Brox (2015).  U-Net: Convolutional Networks for Biomedical Image Segmentation.  [ArXiv:1505.04597](http://arxiv.org/abs/1505.04597)

E. Alvarez-Vanhard, G. Fernandez Garcia, T. Corpetti. Super-Resolution by Fusing Multispectral and Terrain Models: Application to Water Level Mapping. [10.1109/LGRS.2023.3319548](https://doi.org/10.1109/LGRS.2023.3319548)